import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Arrays;

public class Field {

    private final static char DEFAULT_FIELD_VALUE = ' ';

    private Scanner scanner = new Scanner(System.in);

    private final int fieldSize;
    private final int numberOfFieldCells;
    private char[][] field;

    public Field (int fieldSize) {
        this.fieldSize = fieldSize;
        this.numberOfFieldCells = fieldSize * fieldSize;
        field = new char[fieldSize][fieldSize];
        this.setDefaultValues();
    }

    public void showField() {
        System.out.print("    ");
        for (int i = 1; i <= fieldSize; i++) {
            if (i >= 10)
                System.out.print(i + " ");
            else
                System.out.print(i + "  ");
        }
        System.out.println();

        for (int i = 0; i < fieldSize; i++) {
            if (i >= 9)
                System.out.print(i + 1 + " ");
            else
                System.out.print(i + 1 + "  ");

            for (int j = 0; j < fieldSize; j++) {
                System.out.print("[" + field[i][j] + "]");
            }
            System.out.println();
        }
    }

    public void showServerField(PrintWriter out) {    // тоже самое что и showfield только выводит через out (отправлять поле клиенту)
        System.out.print("    ");
        for (int i = 1; i <= fieldSize; i++) {
            if (i >= 10)
                out.print(i + " ");
            else
                out.print(i + "  ");
        }
        out.println();

        for (int i = 0; i < fieldSize; i++) {
            if (i >= 9)
                out.print(i + 1 + " ");
            else
                out.print(i + 1 + "  ");

            for (int j = 0; j < fieldSize; j++) {
                out.print("[" + field[i][j] + "]");
            }
            out.println();
        }
    }

    public void setDefaultValues () {
        for (char[] row : field){
            Arrays.fill(row, DEFAULT_FIELD_VALUE);
        }
    }

    public void setCellValueFromNumber (int number, char value) {
        number--;
        if (number < numberOfFieldCells) {
            for (int i = 0; i < fieldSize; i++) {
                if (number < fieldSize) {
                    field[i][number] = value;
                    return;
                } else {
                    number -= fieldSize;
                }
            }
        } else {
            System.out.print("The entered value is outside of the field, enter a new value3: ");
            setCellValueFromNumber(scanner.nextInt(), value);
        }
    }

    public void setCellValue (int y, int x, char value) {
        field[y][x] = value;
    }

    public char getCellValueFromNumber (int number) {
        char value = ' ';
        number--;
        for (int i = 0; i < fieldSize; i++) {
            if (number < fieldSize) {
                value = field[i][number];
                return value;
            } else {
                number -= fieldSize;
            }
        }
        return value;
    }

    public char getCellValue (int y, int x) {
        return field[y][x];
    }

    public char[][] getField () {
        return field;
    }

    public int getFieldSize () {
        return fieldSize;
    }

    public int getNumberOfFieldCells () {
        return numberOfFieldCells;
    }

    public static char getDefaultFieldValue() {
        return DEFAULT_FIELD_VALUE;
    }
}
