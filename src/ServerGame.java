import java.io.*;
import java.net.*;
import java.util.Scanner;

public class ServerGame extends Game {

    protected int port;

    private BufferedReader in = null;
    private PrintWriter    out= null;

    private ServerSocket servers = null;
    private Socket       fromclient = null;

    public ServerGame (String firstPlayerName, char firstPlayerChar, String secondPlayerName, char secondPlayerChar, int port) {
        field = new Field(FIELD_SIZE);
        numberOfTurns = 1;
        firstPlayer = new Player(firstPlayerName,firstPlayerChar);
        secondPlayer = new Player(secondPlayerName,secondPlayerChar);
        this.port = port;

        try {
            servers = new ServerSocket(4444);
        } catch (IOException e) {
            System.out.println("Couldn't listen to port 4444");
            System.exit(-1);
        }
    }

    public ServerGame ( int port) {
        field = new Field(FIELD_SIZE);
        numberOfTurns = 1;
        this.firstPlayer = new Player("First Player",'X');
        this.secondPlayer = new Player("Second Player",'O');
        this.port = port;
    }

    public void run() throws IOException {
        field.showField();
        gameLoop();
    }

    private void gameLoop() throws IOException {
        for (int i = 0; i < (field.getFieldSize() * field.getFieldSize()); i++) {
            if (numberOfTurns % 2 != 0) {
                currentPlayer = firstPlayer;
                System.out.print("Turn: " + numberOfTurns + " :: " + currentPlayer.getName() + " ");
                turnHuman();
            } else {
                currentPlayer = secondPlayer;
                System.out.println("Turn: " + numberOfTurns + " :: " + currentPlayer.getName());
                if (in == null || out == null){
                    connect();
                }

                turnNetHuman();
            }

            field.showField();
            numberOfTurns++;
            System.out.println();

            if ( winCheck()) {
                System.out.println("GAME OVER! Winner is: " + currentPlayer.getName());
                break;
            }
        }
        if (!winCheck()) {
            System.out.println("Draw");
        }
    }

    private void connect() throws IOException {
        try {
            System.out.print("Waiting for a client...");
            fromclient= servers.accept();
            System.out.println("Client connected");
        } catch (IOException e) {
            System.out.println("Can't accept");
            System.exit(-1);
        }

        in  = new BufferedReader(new
                InputStreamReader(fromclient.getInputStream()));
        out = new PrintWriter(fromclient.getOutputStream(),true);

    }

    private void turnNetHuman() throws IOException {

        Scanner scanner;

        field.showServerField(out);
        out.println("Введите 2 числа");


        while (true) {

            //String  input;
            //input = in.readLine();

            scanner = new Scanner(in);

            int scInY, scInX;
            while (!scanner.hasNextInt()){
                out.println("введена не цифра");
                scanner.next();
            }

            scInY = scanner.nextInt() - 1;
            while (!scanner.hasNextInt()){
                out.println("введена не цифра, попробуйте снова");
                scanner.next();
            }
            scInX = scanner.nextInt() - 1;

            if (scInX < FIELD_SIZE && scInY < FIELD_SIZE && field.getCellValue(scInX, scInY) == Field.getDefaultFieldValue()) {

                field.setCellValue(scInX, scInY, currentPlayer.getPlayerChar());
                field.showServerField(out);
                break;

            } else {
                System.out.println("Field is occupied, enter a different value: ");
                out.println("что то не так введи 2 числа");
                turnNetHuman();
            }

        }


    }

    private void closeConnect() throws IOException{
        out.close();
        in.close();
        fromclient.close();
        servers.close();
    }
}
