public class LocalGame extends Game {

    private int gamemode; // gamemode = 0 player vs ai ; gamemode = 1 player vs player

    public LocalGame (String firstPlayerName, char firstPlayerChar, String secondPlayerName, char secondPlayerChar, int gamemode) {
        field = new Field(FIELD_SIZE);
        numberOfTurns = 1;
        firstPlayer = new Player(firstPlayerName,firstPlayerChar);
        secondPlayer = new Player(secondPlayerName,secondPlayerChar);
        this.gamemode = gamemode;
    }

    public LocalGame () {
        field = new Field(FIELD_SIZE);
        numberOfTurns = 1;
        this.firstPlayer = new Player("Player",'X');
        this.secondPlayer = new Player("AI",'O');
        this.gamemode = 0;

    }

    public void run() {
        field.showField();
        gameLoop();
    }

    private void gameLoop() {
        for (int i = 0; i < (field.getFieldSize() * field.getFieldSize()); i++) {
            if (numberOfTurns % 2 != 0) {
                currentPlayer = firstPlayer;
                System.out.print("Turn: " + numberOfTurns + " :: " + currentPlayer.getName() + " ");
                turnHuman();
            } else {
                currentPlayer = secondPlayer;
                System.out.println("Turn: " + numberOfTurns + " :: " + currentPlayer.getName());
                if (gamemode == 0){
                    turnAi();
                } else {
                    turnHuman();
                }
            }

            field.showField();
            numberOfTurns++;
            System.out.println();

            if ( winCheck()) {
                System.out.println("GAME OVER! Winner is: " + currentPlayer.getName());
                break;
            }
        }
        if (!winCheck()) {
            System.out.println("Draw");
        }
    }

    protected void turnAi() {
        int rand = random.nextInt(field.getNumberOfFieldCells() - 1) + 1;
        if (field.getCellValueFromNumber(rand) == Field.getDefaultFieldValue()) {
            field.setCellValueFromNumber(rand, currentPlayer.getPlayerChar());
        } else {
            turnAi();
        }
    }
}
