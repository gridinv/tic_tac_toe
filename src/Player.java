
public class Player {

    private String name;
    private char playerChar;

    public Player (String name, char playerChar) {
        this.name = name;
        this.playerChar = playerChar;
    }

    public void setChar(char playerChar) {     //такой сетчар (без сканера и вывода "введите ченить") более универсален и независим
        this.playerChar = playerChar;
    }

    public char getPlayerChar () {
        return playerChar;
    }

    public String getName () {
        return name;
    }
}
