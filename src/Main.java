import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main (String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Выберите режим игры");
        System.out.println("1. Игра с компьютером");
        System.out.println("2. Игра вдвоем на одном компьютере");
        System.out.println("3. Создать хост сессию");
        System.out.println("4. Подключиться к игре");

        int choise = 3;//scanner.nextInt();

        switch (choise){

            case 1:{
                LocalGame game = new LocalGame();
                game.run();
                break;
            }
            case 2:{

                LocalGame game = new LocalGame("vova",'x',"vitalik",'o','1');
                game.run();
                break;
            }
            case 3:{
                int port = 4444;
                ServerGame game = new ServerGame("vova",'x',"vitalik",'o',port);
                game.run();
                break;

            }
            case 4:{

            }
        }

    }

}
