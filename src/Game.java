import java.util.Random;
import java.util.Scanner;

public class Game {

    private static final int CHARS_IN_LINE_TO_WIN = 5;
    public static final int FIELD_SIZE = 15;

    private static final int LINES_CHECK_FALSE = 0;
    private static final int LINES_CHECK_TRUE = 1;
    private static final int LINES_CHECK_EMPTY_CELL = 2;
    private static final int LINES_CHECK_CONTUNE_CYCLE = 3;

    private Scanner scanner = new Scanner(System.in);
    protected Field field;
    protected int numberOfTurns;
    protected Player currentPlayer;

    protected Player firstPlayer;
    protected Player secondPlayer;


    protected Random random = new Random();

    public Game (String firstPlayerName, char firstPlayerChar, String secondPlayerName, char secondPlayerChar, int gamemode) {
        field = new Field(FIELD_SIZE);
        numberOfTurns = 1;
        firstPlayer = new Player(firstPlayerName,firstPlayerChar);
        secondPlayer = new Player(secondPlayerName,secondPlayerChar);
    }

    public Game () {
        field = new Field(FIELD_SIZE);
        numberOfTurns = 1;
        this.firstPlayer = new Player("Player",'X');
        this.secondPlayer = new Player("AI",'O');
    }
    
    public Field getField(){
        return field;
    }

    protected void turnHuman() {
        int scInY, scInX;
        while (!scanner.hasNextInt()){
            System.out.println("введена не цифра");
            scanner.next();
        }
        scInY = scanner.nextInt() - 1;
        while (!scanner.hasNextInt()){
            System.out.println("введена не цифра, попробуйте еще раз");
            scanner.next();
        }
        scInX = scanner.nextInt() - 1;

        if (scInX < FIELD_SIZE && scInY < FIELD_SIZE && field.getCellValue(scInX, scInY) == Field.getDefaultFieldValue()) {

            field.setCellValue(scInX, scInY, currentPlayer.getPlayerChar());

        } else {

            System.out.println("Field is occupied, enter a different value: ");
            turnHuman();
        }


    }



    protected boolean winCheck() {
        for (int i = 0; i < field.getFieldSize(); i++) {
            for (int j = 0; j < field.getFieldSize(); j++) {
                switch (linesCheck(i, j)) {
                    case LINES_CHECK_TRUE: return true;
                    case LINES_CHECK_FALSE: return false;
                }
            }
        }
        return false;
    }

    private int linesCheck(int y, int x) {
        if (field.getCellValue(y, x) == Field.getDefaultFieldValue()) {
            return LINES_CHECK_EMPTY_CELL;
        }

        char cellChar = field.getCellValue(y, x);

        try {
            for (int i = 0; i < CHARS_IN_LINE_TO_WIN; i++) {
                if (field.getCellValue(y, x + i) == cellChar)
                    break;
                if (i == CHARS_IN_LINE_TO_WIN - 1)
                    return LINES_CHECK_TRUE;
            }
        } catch (Exception e) {}

        try {
            for (int i = 0; i < CHARS_IN_LINE_TO_WIN; i++) {
                if (field.getCellValue(y, x - i) != cellChar)
                    break;
                if (i == CHARS_IN_LINE_TO_WIN - 1)
                    return LINES_CHECK_TRUE;
            }
        } catch (Exception e) {}

        try {
            for (int i = 0; i < CHARS_IN_LINE_TO_WIN; i++) {
                if (field.getCellValue(y + i, x) != cellChar)
                    break;
                if (i == CHARS_IN_LINE_TO_WIN - 1)
                    return LINES_CHECK_TRUE;
            }
        } catch (Exception e) {}

        try {
            for (int i = 0; i < CHARS_IN_LINE_TO_WIN; i++) {
                if (field.getCellValue(y - i, x) != cellChar)
                    break;
                if (i == CHARS_IN_LINE_TO_WIN - 1)
                    return LINES_CHECK_TRUE;
            }
        } catch (Exception e) {}

        try {
            for (int i = 0; i < CHARS_IN_LINE_TO_WIN; i++) {
                if (field.getCellValue(y + i, x + i) != cellChar)
                    break;
                if (i == CHARS_IN_LINE_TO_WIN - 1)
                    return LINES_CHECK_TRUE;
            }
        } catch (Exception e) {}

        try {
            for (int i = 0; i < CHARS_IN_LINE_TO_WIN; i++) {
                if (field.getCellValue(y - i, x + i) != cellChar)
                    break;
                if (i == CHARS_IN_LINE_TO_WIN - 1)
                    return LINES_CHECK_TRUE;
            }
        } catch (Exception e) {}

        try {
            for (int i = 0; i < CHARS_IN_LINE_TO_WIN; i++) {
                if (field.getCellValue(y - i, x - i) != cellChar)
                    break;
                if (i == CHARS_IN_LINE_TO_WIN - 1)
                    return LINES_CHECK_TRUE;
            }
        } catch (Exception e) {}

        try {
            for (int i = 0; i < CHARS_IN_LINE_TO_WIN; i++) {
                if (field.getCellValue(y + i, x - i) != cellChar)
                    break;
                if (i == CHARS_IN_LINE_TO_WIN - 1)
                    return LINES_CHECK_TRUE;
            }
        } catch (Exception e) {}

        return LINES_CHECK_CONTUNE_CYCLE;
    }
}
